﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace test957.DTO
{
    public class UserTestHystoryDTO
    {
        public int Id { get; set; }
        public DateTime DateTime { get; set; }
        public int Result { get; set; }
        public string TestName { get; set; }
        public string UserLogin { get; set; }
    }
}
