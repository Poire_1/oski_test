﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace test957.DTO
{
    public class AnswerDTO
    {
        public string Content { get; set; }
        public int QuestionId { get; set; }
        public bool Right_Wrong { get; set; }
    }
}
