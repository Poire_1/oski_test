﻿using DB.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace test957.DTO
{
    public class DTOconverter
    {
        public static UserTestHystoryDTO ToUserTestHystoryDTO(UserTestHystory userTestHystory)
        {
            return new UserTestHystoryDTO()
            {
                Id = userTestHystory.Id,
                DateTime = userTestHystory.DateTime,
                Result = userTestHystory.Result,
                TestName = userTestHystory.Test?.TestName, 
                UserLogin = userTestHystory.Person?.Login               
            };
        }

        public static QuestionDTO ToQuestionDTO(Question question)
        {
            return new QuestionDTO()
            {
                Id = question.Id,
                Content = question.Content,
                Answers = question.Answers?.Select(x => ToAnswerDTO(x))
            };
        }

        public static AnswerDTO ToAnswerDTO(Answer answer)
        {
            return new AnswerDTO
            {
                Content = answer.Content,
                QuestionId = answer.QuestionId,
                Right_Wrong = answer.Right_Wrong
            };
        }

        public static TestDTO ToTestsDTO(Test test)
        {
            ICollection<Question> question = test.Questions ?? null;
            return new TestDTO()
            {
                Id = test.Id,
                TestName = test.TestName,
                Question = question != null ? question.Select(f => ToQuestionDTO(f)) : null,
                Topics = question != null ? question.Select(f => f.Topic.TopicName) as string[] : null
            };
        }
    }
}
