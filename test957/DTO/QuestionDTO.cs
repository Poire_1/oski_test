﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace test957.DTO
{
    public class QuestionDTO
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public virtual IEnumerable<AnswerDTO> Answers { get; set; }
    }
}
