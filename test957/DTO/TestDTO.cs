﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace test957.DTO
{
    public class TestDTO
    {
        public int Id { get; set; }
        public string TestName { get; set; }
        public string[] Topics { get; set; }
        public IEnumerable<QuestionDTO> Question { get; set;}
    }
}


