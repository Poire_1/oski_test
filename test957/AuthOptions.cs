﻿using DB;
using DB.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace test957
{
    public interface IAuth
    {
        void Authorization(string username, string password, out string tokenString);
    }

    class JWTAuth : IAuth
    {
        private IRepository _repository;

        public JWTAuth(IRepository repository)
        {
            _repository = repository;
        }
        public void Authorization(string username, string password, out string tokenString)
        {
            ClaimsIdentity user = GetIdentity(username, password);
            if (user != null)
            {
                tokenString = GenerateJSONWebToken(user);
            }
            else { tokenString = null; }
        }

        private string GenerateJSONWebToken(ClaimsIdentity user)
        {
            DateTime now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
            issuer: AuthOptions.ISSUER,
            audience: AuthOptions.AUDIENCE,
            notBefore: now,
            claims: user.Claims,
            expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
            signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }

        private ClaimsIdentity GetIdentity(string username, string password)
        {
            Person person = _repository.ListWithParam<Person>(x => x.Login == username && x.Password == password).FirstOrDefault();
            if (person != null)
            {
                var claims = new List<Claim>
                 {
                     new Claim(ClaimsIdentity.DefaultNameClaimType, person.Login),
                     new Claim(ClaimsIdentity.DefaultRoleClaimType, person.Role),
                 };
                ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }
            return null;
        }
    }

    public class AuthOptions//move to configuration
    {
        public const string ISSUER = "TestServerTest";
        public const string AUDIENCE = "OSKYClientTest";
        const string KEY = "OSKYsupersecretSecretkeyTest";
        public const int LIFETIME = 1;
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
