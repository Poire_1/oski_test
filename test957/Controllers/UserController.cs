﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DB;
using DB.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;


namespace test957.Controllers
{
    [Route("user/[controller]")]
    [ApiController]
    public class UserController : ControllerBase//No error handler!!!!!!!!!!!!!!
    {
        private IRepository _repository;

        public UserController(IRepository repository)
        {
            _repository = repository;
        }

        [Authorize(Roles = "user")]
        [HttpPost("/viewTestHystory")]
        public async Task ViewTestHystory(int testId)
        {
            var userTestHystory = _repository.ListWithParam<UserTestHystory>(x => x.TestId == testId && x.Person.Login == User.Identity.Name).OrderBy(s => s.DateTime);
            await Response.WriteAsync(JsonConvert.SerializeObject( userTestHystory.Select(x=> DTO.DTOconverter.ToUserTestHystoryDTO(x)), new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "user")]
        [HttpPost("/viewAllTests")]
        public async Task ViewAllTests(int itemsNumPerPage, int pageNumber)
        {
            //var tests = _repository.List<Test>();
            var tests = _repository.List_Pagination<Test>(itemsNumPerPage, pageNumber);
            await Response.WriteAsync(JsonConvert.SerializeObject(tests.Select(x=> DTO.DTOconverter.ToTestsDTO(x)), new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "user")]
        [HttpPost("/saveLastResult")]
        public async Task SaveLastResult(int testId, int result)
        {//no
            var response = _repository.Add(new UserTestHystory() { DateTime = DateTime.UtcNow, TestId = testId, Result = result, Person = _repository.ListWithParam<Person>(p => p.Login == User.Identity.Name).FirstOrDefault() });
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }
    }
}