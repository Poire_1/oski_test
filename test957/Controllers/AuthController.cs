﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using DB;
using DB.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace test957.Controllers
{
    [Route("auth/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private IAuth auth_;

        public AuthController(IAuth auth)
        {
            auth_ = auth;
        }

        [AllowAnonymous]
        [HttpPost("/token")]
        public async Task Token(string username, string password)
        {            
            string tokenString;
            auth_.Authorization(username, password, out tokenString);
            await Response.WriteAsync(JsonConvert.SerializeObject(tokenString != null ? Ok(new { token = tokenString }) as IActionResult : Unauthorized() as IActionResult, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }
        
    }
}