﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DB;
using DB.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace test957.Controllers
{
    [Route("admin/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase //No error handler!!!!!!!!!!!!!!
    {
        private IRepository _repository;

        public AdminController(IRepository repository)
        {
            _repository = repository;
        } 

        [Authorize(Roles = "admin")]
        [HttpPost("/addTopic")]
        public async Task AddTopic([FromQuery] Topic topic)
        {
            _repository.Add(topic);
            await Response.WriteAsync(JsonConvert.SerializeObject("Topic successfully added", new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "admin")]
        [HttpPost("/addTest")]
        public async Task AddTest([FromQuery] Test test)
        {
            _repository.Add(test);
            await Response.WriteAsync(JsonConvert.SerializeObject("Test successfully added", new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "admin")]
        [HttpPost("/addQuestion")]
        public async Task AddQuestion([FromQuery] Question question)
        {
            _repository.Add(question);
            await Response.WriteAsync(JsonConvert.SerializeObject("Question successfully added", new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "admin")]
        [HttpPost("/addAnswers")]
        public async Task AddAnswers([FromQuery] IQueryable<Answer> answers)
        {
            _repository.AddRange(answers);
            await Response.WriteAsync(JsonConvert.SerializeObject("Question successfully added", new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }
    }
}