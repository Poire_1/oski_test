﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DB
{
    public interface IRepository 
    {
        #region Lists  
        IQueryable<T> ListWithParam<T>(Expression<Func<T, bool>> predicate) where T : EntityBase;
        IQueryable<T> ListWithParam_Pagination<T>(Expression<Func<T, bool>> predicate, int itemsNumPerPage, int pageNumber) where T : EntityBase;
        IQueryable<T> List<T>() where T : EntityBase;
        IQueryable<T> List_Pagination<T>(int itemsNumPerPage, int pageNumber) where T : EntityBase;
        #endregion 
        #region Add
        T Add<T>(T entity) where T : EntityBase;
        IQueryable<T> AddRange<T>(IQueryable<T> entity) where T : EntityBase;
        #endregion 
        #region Remove
        T Remove<T>(T entity) where T : EntityBase;   
        IQueryable<T> RemoveRange<T>(IQueryable<T> entity) where T : EntityBase;
        #endregion
        T GetById<T>(int id) where T : EntityBase;
        //void Edit(T entity);
    }

    public abstract class EntityBase
    {
        public int Id { get; protected set; }
    }
}
