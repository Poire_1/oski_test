﻿using DB.Config;
using DB.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DB
{
    public class DBcontext :  DbContext
    {
        public DbSet<Person> Persons { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<UserTestHystory> UserTestHystores { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set;}

        public DBcontext(DbContextOptions<DBcontext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyConfiguration(new PersonConfig());
            builder.ApplyConfiguration(new TestConfig());
            builder.ApplyConfiguration(new UserTestHystoryConfig());
            builder.ApplyConfiguration(new TopicConfig());
            builder.ApplyConfiguration(new QuestionConfig());
            builder.ApplyConfiguration(new AnswerConfig());
        }
    }
}
