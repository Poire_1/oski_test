﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace DB.Model
{
    public class Topic : EntityBase
    {
        [MaxLength(20)]
        [Required]
        public string TopicName { get; set; }//unique
        public virtual ICollection<Question> Questions { get; set; }
    }  
}
