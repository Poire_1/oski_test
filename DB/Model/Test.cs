﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace DB.Model
{
    public class Test : EntityBase
    {
        [MaxLength(60)]
        [Required]
        public string TestName { get; set; }
        public virtual ICollection<UserTestHystory> UserTestHystory { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
    }  
}
