﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DB.Model
{
    public class UserTestHystory : EntityBase
    {
        [Required]
        public DateTime DateTime { get; set; }
        [Required]
        public int Result { get; set; }
        [Required]
        public int TestId { get; set; }
        public virtual Test Test { get; set; }
        [Required]
        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
    }
}
