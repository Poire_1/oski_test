﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace DB.Model
{
    public class Question : EntityBase
    {
        [MaxLength(200)]
        [Required]
        public string Content { get; set; }//unique
        [Required]
        public int TopicId { get; set; }
        public virtual Topic Topic { get; set; }
        [Required]
        public int TestId { get; set; }
        public virtual Test Test { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
    } 
}
