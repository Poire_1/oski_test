﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace DB.Model
{
    public class Person : EntityBase
    {
        [MaxLength(30)]
        [Required]
        public string Login { get; set; }

        [MaxLength(30)]
        [Required]
        public string Password { get; set; }

        [MaxLength(30)]
        [Required]
        public string Role { get; set; }
        public virtual IQueryable<UserTestHystory> UserTestHystory { get; set; }
    }
}