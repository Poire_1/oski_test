﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace DB.Model
{
    public class Answer : EntityBase
    {
        [MaxLength(200)]
        [Required]
        public string Content { get; set; }
        [Required]
        public int QuestionId { get; set; }
        public virtual Question Question { get; set; }
        private bool _right_wrong;
        public bool Right_Wrong
        {
            get { return _right_wrong; }
            set
            {
                if (!value && this.Question.Answers.Any(j => j.Right_Wrong == true))
                {
                    throw new OneCorrectAnswerException("There can be only one correct answer");
                }
                _right_wrong = value;
            }
        }
    }

    public class OneCorrectAnswerException : Exception
    {
        public OneCorrectAnswerException(string message) : base(message)
        {
        }
    }
}
