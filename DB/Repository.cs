﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DB
{
    public class Repository : IRepository
    {
        private DBcontext _dbContext;
        public Repository(DBcontext dbContext)
        {
            _dbContext = dbContext;
        }
        #region Lists 
        public IQueryable<T> List<T>() where T : EntityBase
        {
            return _dbContext.Set<T>();
        }

        public IQueryable<T> ListWithParam<T>(Expression<Func<T, bool>> predicate) where T : EntityBase
        {
            return _dbContext.Set<T>().Where(predicate);
        }

        public IQueryable<T> ListWithParam_Pagination<T>(Expression<Func<T, bool>> predicate, int itemsNumPerPage, int pageNumber) where T : EntityBase
        {
            return _dbContext.Set<T>().Where(predicate).OrderBy(g => g.Id).Skip((pageNumber - 1) * itemsNumPerPage).Take(itemsNumPerPage);
        }

        public IQueryable<T> List_Pagination<T>(int itemsNumPerPage, int pageNumber) where T : EntityBase
        {
            return _dbContext.Set<T>().OrderBy(g => g.Id).Skip((pageNumber - 1) * itemsNumPerPage).Take(itemsNumPerPage);
        }
        #endregion 
        #region Add
        public T Add<T>(T entity) where T : EntityBase
        {
            _dbContext.Set<T>().Add(entity);
            _dbContext.SaveChanges();
            return entity;
        }

        public IQueryable<T> AddRange<T>(IQueryable<T> entity) where T : EntityBase
        {
            _dbContext.Set<T>().AddRange(entity);
            _dbContext.SaveChanges();
            return entity;
        }
        #endregion 
        #region Remove
        public T Remove<T>(T entity) where T : EntityBase
        {
            _dbContext.Set<T>().Remove(entity);
            _dbContext.SaveChanges();
            return entity;
        }
       
        public IQueryable<T> RemoveRange<T>(IQueryable<T> entity) where T : EntityBase
        {
            _dbContext.Set<T>().RemoveRange(entity);
            _dbContext.SaveChanges();
            return entity;
        }
        #endregion
        public T GetById<T>(int id) where T : EntityBase
        {
            return _dbContext.Set<T>().Find(id);
        }

        
    }
}
