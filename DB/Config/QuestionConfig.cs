﻿using DB.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DB.Config
{
    class QuestionConfig : IEntityTypeConfiguration<Question>
    {
        public void Configure(EntityTypeBuilder<Question> x)
        {
            x.HasIndex(y => new { y.Content, y.TestId, y.TopicId }).IsUnique();
            x.HasOne(y => y.Topic).WithMany(h => h.Questions).HasForeignKey(fk => fk.TopicId);
            x.HasOne(y => y.Test).WithMany(h => h.Questions).HasForeignKey(fk => fk.TestId);
        }
    }
}
