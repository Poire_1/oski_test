﻿using DB.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DB.Config
{
    class AnswerConfig : IEntityTypeConfiguration<Answer>
    {
        public void Configure(EntityTypeBuilder<Answer> x)
        {
            x.HasIndex(y => new { y.Content, y.QuestionId }).IsUnique();
        }
    }
}