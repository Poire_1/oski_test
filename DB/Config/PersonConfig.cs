﻿using DB.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DB.Config
{
    public class PersonConfig : IEntityTypeConfiguration<Person>
    {
        public void Configure(EntityTypeBuilder<Person> x)
        {
            x.HasIndex(e => e.Login).IsUnique();
            x.HasAlternateKey(o => new { o.Login, o.Password, o.Role });
        }
    }
}
