﻿using DB.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DB.Config
{
    class UserTestHystoryConfig : IEntityTypeConfiguration<UserTestHystory>
    {
        public void Configure(EntityTypeBuilder<UserTestHystory> x)
        {
            x.HasIndex(y => new { y.TestId, y.Result, y.PersonId, y.DateTime }).IsUnique();
            x.HasOne(y => y.Person).WithMany(h => h.UserTestHystory).HasForeignKey(fk => fk.PersonId);
            x.HasOne(y => y.Test).WithMany(h=>h.UserTestHystory).HasForeignKey(fk => fk.TestId);
        }
    }
}
